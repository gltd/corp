globally.ltd
895 Broadway #4
Brooklyn, NY 11211 

## globally.ltd Release Agreement

### What is globally.ltd?

Each globally.ltd release comprises one or more tracks presented through an interactive website. The sites are collaborative efforts between the artist(s) and the label. We believe that they are a novel alternative to experiencing music online.

### Where and how will my release be available for streaming and purchase?

globally.ltd will upload your music to our Bandcamp page as an album, our Soundcloud page as a playlist, and to Youtube as a screen-cap based
explicit approval to upload your music to any additional platforms. 
You are also free to sell and distribute the release through your own channels.

### How / when do I get paid?

globally.ltd will sell your release exclusively through Bandcamp, though we may eventually create our own digital store. You are free to sell your music through your own Bandcamp or any other digital distribution platform. We will split whatever we sell through our channels 50/50. You keep whatever you sell through your own channels. Royalties will be distributed on a quarterly basis (January 1, April 1, July 1, October 1)

### Who owns what?

We ask for a non-exclusive license to sell and distribute your music as outlined above. 
You own your masters and licensing rights and can resell or re-release your music however you see fit.

### What if another label wants to put out this release?

That’s great! All we ask is that your working with another label not necessitate us taking our release off of our distribution channels. We want you to be able to pursue whatever opportunities may come your way, but we also want to maintain the integrity of the work that we do together.

### Do you handle mastering?

Yes! We'll be working with {{MASTERING_ENGINEER_NAME}} on all your releases and cover their costs.

### When will my record come out? 

From the point you deliver the final mixes, it usually takes 3-4 months until the music is released on the above channels.  

### How is the album art created? 

We generate the album art from screenshots of the websites we create. We’ll work closely with you to select an image you like

### How will you publicize the release?

We will issue a press release for your album about one month before the release date. This will go out to a list we maintain of journalists, djs, friends, family members, and heroes. We can also include and emails lists you may maintain.We will work with press outlets to premiere your tracks on their channels before the release date. We will publish clips of your release on our Instagram, Facebook, and Twitter accounts.

### What’s my advance? 

We can offer you a {{ADVANCE_AMOUNT}} advance upfront for the completion of a {{ RELEASE_TYPE }}. 



If all this sounds good to you, we’d love to work with you. Let’s make something beautiful together.


With love, honor, and mutual trust,

                                                {{CONTRACT_DATE}} {{ARTIST_NAME}}, Artist
 
                                                {{CONTRACT_DATE}} Brian Abelson, globally.ltd



